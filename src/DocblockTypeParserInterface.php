<?php

namespace Serenata\DocblockTypeParser;

/**
 * Parses docblock type specifications into more usable objects.
 *
 * @see https://phpdoc.org/docs/latest/references/phpdoc/types.html
 */
interface DocblockTypeParserInterface
{
    /**
     * @param string $specification
     *
     * @return DocblockType
     */
    public function parse(string $specification): DocblockType;
}
