<?php

namespace Serenata\DocblockTypeParser;

/**
 * Represents a float as docblock type.
 *
 * {@inheritDoc}
 */
class FloatDocblockType extends SpecialDocblockType
{
    /**
     * @var string
     */
    public const STRING_VALUE = 'float';

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return self::STRING_VALUE;
    }
}
