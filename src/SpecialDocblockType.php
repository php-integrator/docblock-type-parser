<?php

namespace Serenata\DocblockTypeParser;

/**
 * Represents a special (non-class) docblock type.
 *
 * {@inheritDoc}
 */
abstract class SpecialDocblockType extends SingleDocblockType
{

}
