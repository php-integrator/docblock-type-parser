<?php

namespace Serenata\DocblockTypeParser;

/**
 * Represents an int as docblock type.
 *
 * {@inheritDoc}
 */
class IntegerDocblockType extends IntDocblockType
{
    /**
     * @var string
     */
    public const STRING_VALUE = 'integer';

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return self::STRING_VALUE;
    }
}
