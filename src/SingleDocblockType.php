<?php

namespace Serenata\DocblockTypeParser;

/**
 * Represents a single (non-compound) docblock type.
 *
 * This is a value object and immutable.
 */
abstract class SingleDocblockType extends DocblockType
{

}
