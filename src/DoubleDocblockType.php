<?php

namespace Serenata\DocblockTypeParser;

/**
 * Represents a float as docblock type.
 *
 * {@inheritDoc}
 */
class DoubleDocblockType extends FloatDocblockType
{
    /**
     * @var string
     */
    public const STRING_VALUE = 'double';

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return self::STRING_VALUE;
    }
}
