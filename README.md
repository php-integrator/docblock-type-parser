# Serenata - Docblock Type Parser
Converts docblock types or type specifications into a more usable tree of type objects.

This project falls under the [Serenata](https://gitlab.com/Serenata) umbrella.

## Installation
```sh
composer require serenata/docblock-type-parser
```

## Usage
```php
<?php

use Serenata\DocblockTypeParser\DocblockTypeParser;
use Serenata\DocblockTypeParser\CompoundDocblockType;

$parser = new DocblockTypeParser();
$type = $parser->parse('(int|bool)[]|string[]|array|null');

assert($type instanceof CompoundDocblockType);
assert($type->toString() === '(int|bool)[]|string[]|array|null');
```

## Docblock Types?
Docblock types are [those types that you specify inside docblocks](https://phpdoc.org/docs/latest/references/phpdoc/types.html) for parameters, return values and other things. They allow narrowing down the type of a parameter that would otherwise not be possible in PHP itself and their syntax is more complex as a result.

Docblock types can can be interpreted by various tools, such as Serenata or documentation generators:

```php
<?php

/**
 * @param (int|bool)[]|string[]|array|null $test
 */
public function foo(array $test)
{

}
```

## Comparing
You can use the class `DocblockTypeEquivalenceComparator` to compare one type with another. It will indicate if they are equivalent or not. This will simply check if both nodes - or hierarchies in the case of compound types - are either identical or contain the same types, albeit in a different order (hence they are still semantically equivalent).

![GPLv3 Logo](https://gitlab.com/Serenata/Serenata/raw/793c93b0f69a5f4ba183f1dfff79f0c68d9bd010/resources/images/gpl_v3.png)
