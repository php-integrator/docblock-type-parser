<?php

namespace spec\Serenata\DocblockTypeParser;

use Serenata\DocblockTypeParser;

use PhpSpec\ObjectBehavior;

class DocblockTypeParserSpec extends ObjectBehavior
{
    /**
     * @return void
     */
    public function it_converts_string_into_node(): void
    {
        $this->parse(DocblockTypeParser\StringDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\StringDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_int_into_node(): void
    {
        $this->parse(DocblockTypeParser\IntDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\IntDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_integer_into_node(): void
    {
        $this->parse(DocblockTypeParser\IntegerDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\IntegerDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_bool_into_node(): void
    {
        $this->parse(DocblockTypeParser\BoolDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\BoolDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_boolean_into_node(): void
    {
        $this->parse(DocblockTypeParser\BooleanDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\BooleanDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_float_into_node(): void
    {
        $this->parse(DocblockTypeParser\FloatDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\FloatDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_double_into_node(): void
    {
        $this->parse(DocblockTypeParser\DoubleDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\DoubleDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_object_into_node(): void
    {
        $this->parse(DocblockTypeParser\ObjectDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\ObjectDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_mixed_into_node(): void
    {
        $this->parse(DocblockTypeParser\MixedDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\MixedDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_array_into_node(): void
    {
        $this->parse(DocblockTypeParser\ArrayDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\ArrayDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_resource_into_node(): void
    {
        $this->parse(DocblockTypeParser\ResourceDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\ResourceDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_void_into_node(): void
    {
        $this->parse(DocblockTypeParser\VoidDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\VoidDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_null_into_node(): void
    {
        $this->parse(DocblockTypeParser\NullDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\NullDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_callable_into_node(): void
    {
        $this->parse(DocblockTypeParser\CallableDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\CallableDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_false_into_node(): void
    {
        $this->parse(DocblockTypeParser\FalseDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\FalseDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_true_into_node(): void
    {
        $this->parse(DocblockTypeParser\TrueDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\TrueDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_self_into_node(): void
    {
        $this->parse(DocblockTypeParser\SelfDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\SelfDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_static_into_node(): void
    {
        $this->parse(DocblockTypeParser\StaticDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\StaticDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_this_into_node(): void
    {
        $this->parse(DocblockTypeParser\ThisDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\ThisDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_iterable_into_node(): void
    {
        $this->parse(DocblockTypeParser\IterableDocblockType::STRING_VALUE)->shouldBeLike(new DocblockTypeParser\IterableDocblockType());
    }

    /**
     * @return void
     */
    public function it_converts_class_type_into_node(): void
    {
        $this->parse('\A\B\C')->shouldBeLike(new DocblockTypeParser\ClassDocblockType('\A\B\C'));
    }

    /**
     * @return void
     */
    public function it_converts_specialized_array_into_node(): void
    {
        $this->parse('int[]')->shouldBeLike(new DocblockTypeParser\SpecializedArrayDocblockType(
            new DocblockTypeParser\IntDocblockType()
        ));
    }

    /**
     * @return void
     */
    public function it_converts_compound_types_into_node(): void
    {
        $this->parse('int|\A|B|null')->shouldBeLike(
            new DocblockTypeParser\CompoundDocblockType(
                new DocblockTypeParser\IntDocblockType(),
                new DocblockTypeParser\ClassDocblockType('\A'),
                new DocblockTypeParser\ClassDocblockType('B'),
                new DocblockTypeParser\NullDocblockType()
            )
        );
    }

    /**
     * @return void
     */
    public function it_ignores_outer_parantheses(): void
    {
        $this->parse('(int|null)')->shouldBeLike(
            new DocblockTypeParser\CompoundDocblockType(
                new DocblockTypeParser\IntDocblockType(),
                new DocblockTypeParser\NullDocblockType()
            )
        );
    }

    /**
     * @return void
     */
    public function it_converts_paranthesized_specialized_array_into_node(): void
    {
        $this->parse('(int|bool)[]')->shouldBeLike(
            new DocblockTypeParser\SpecializedArrayDocblockType(
                new DocblockTypeParser\CompoundDocblockType(
                    new DocblockTypeParser\IntDocblockType(),
                    new DocblockTypeParser\BoolDocblockType()
                )
            )
        );
    }

    /**
     * @return void
     */
    public function it_converts_compound_type_with_complex_parts_correctly(): void
    {
        $this->parse('(int|bool)[]|string[]|array|null')->shouldBeLike(
            new DocblockTypeParser\CompoundDocblockType(
                new DocblockTypeParser\SpecializedArrayDocblockType(
                    new DocblockTypeParser\CompoundDocblockType(
                        new DocblockTypeParser\IntDocblockType(),
                        new DocblockTypeParser\BoolDocblockType()
                    )
                ),
                new DocblockTypeParser\SpecializedArrayDocblockType(
                    new DocblockTypeParser\StringDocblockType()
                ),
                new DocblockTypeParser\ArrayDocblockType(),
                new DocblockTypeParser\NullDocblockType()
            )
        );
    }

    /**
     * @return void
     */
    public function it_automatically_removes_leading_compound_type_separators(): void
    {
        $this->parse('|string')->shouldBeLike(new DocblockTypeParser\StringDocblockType());
    }

    /**
     * @return void
     */
    public function it_automatically_removes_trailing_compound_type_separators(): void
    {
        $this->parse('string|')->shouldBeLike(new DocblockTypeParser\StringDocblockType());
    }

    /**
     * @return void
     */
    public function it_automatically_removes_duplicate_compound_type_separators(): void
    {
        $this->parse('int||||string')->shouldBeLike(
            new DocblockTypeParser\CompoundDocblockType(
                new DocblockTypeParser\IntDocblockType(),
                new DocblockTypeParser\StringDocblockType()
            )
        );
    }
}
