<?php

namespace spec\Serenata\DocblockTypeParser;

use Serenata\DocblockTypeParser;

use PhpSpec\ObjectBehavior;

class CompoundDocblockTypeSpec extends ObjectBehavior
{
    /**
     * @return void
     */
    public function it_clones_parts_when_cloned(): void
    {
        $input = [
            new DocblockTypeParser\StringDocblockType(),
            new DocblockTypeParser\NullDocblockType()
        ];

        $this->beConstructedWith(...$input);
        $this->__clone();

        $this->getParts()->shouldBeArrayWithItemsLike($input);
        $this->getParts()->shouldBeArrayWithItemsOfDifferentIdentity($input);
    }

    /**
     * @inheritDoc
     */
    public function getMatchers(): array
    {
        return [
            'beArrayWithItemsLike' => function ($subject, $comparison) {
                foreach ($subject as $key => $item) {
                    if ($comparison[$key] != $item) {
                        return false;
                    }
                }

                return true;
            },

            'beArrayWithItemsOfDifferentIdentity' => function ($subject, $comparison) {
                foreach ($subject as $key => $item) {
                    if ($comparison[$key] === $item) {
                        return false;
                    }
                }

                return true;
            }
        ];
    }
}
