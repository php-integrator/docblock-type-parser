<?php

namespace spec\Serenata\DocblockTypeParser;

use LogicException;

use Serenata\DocblockTypeParser;

use PhpSpec\ObjectBehavior;

class DocblockTypeTransformerSpec extends ObjectBehavior
{
    /**
     * @return void
     */
    public function it_transforms_docblock_types(): void
    {
        $docblockType = new DocblockTypeParser\ClassDocblockType('A');

        $transformedDocblockType = new DocblockTypeParser\ClassDocblockType('\N\A');

        $closure = function (DocblockTypeParser\DocblockType $type) use ($transformedDocblockType): DocblockTypeParser\DocblockType {
            return $transformedDocblockType;
        };

        $this->transform($docblockType, $closure)->shouldBe($transformedDocblockType);
    }

    /**
     * @return void
     */
    public function it_transforms_compound_docblock_type_trees(): void
    {
        $docblockType = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\ClassDocblockType('A'),
            new DocblockTypeParser\ClassDocblockType('B')
        );

        $transformedDocblockType = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\ClassDocblockType('\N\A'),
            new DocblockTypeParser\ClassDocblockType('\N\B')
        );

        $closure = function (DocblockTypeParser\DocblockType $type) use ($docblockType, $transformedDocblockType): DocblockTypeParser\DocblockType {
            if ($type instanceof DocblockTypeParser\ClassDocblockType) {
                return new DocblockTypeParser\ClassDocblockType('\N\\' . $type->getName());
            }

            return $type;
        };

        $this->transform($docblockType, $closure)->shouldBeLike($transformedDocblockType);
    }
}
