<?php

namespace spec\Serenata\DocblockTypeParser;

use Serenata\DocblockTypeParser;

use PhpSpec\ObjectBehavior;

class SpecializedArrayDocblockTypeSpec extends ObjectBehavior
{
    /**
     * @return void
     */
    function it_outputs_string_representation(): void
    {
        $this->beConstructedWith(new DocblockTypeParser\IntDocblockType());

        $this->toString()->shouldReturn('int[]');
    }

    /**
     * @return void
     */
    function it_outputs_string_representation_enclosed_by_parantheses_for_compound_types(): void
    {
        $this->beConstructedWith(
            new DocblockTypeParser\CompoundDocblockType(
                new DocblockTypeParser\IntDocblockType(),
                new DocblockTypeParser\FloatDocblockType()
            )
        );

        $this->toString()->shouldReturn('(int|float)[]');
    }
}
