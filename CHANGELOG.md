## 0.4.0
* Add `DocblockTypeEquivalenceComparator` to compare objects

## 0.3.1
* Fix incorrect package name

## 0.3.0
* Rename namespaces to Serenata
* Update (development) dependencies
* Update deprecated license identifier

## 0.2.0
* Duplicate compound type separators are not ignored.
* Cloning `CompoundDocblockType` will now also clone their children.
* A rudimentary `DocblockTypeTransformerSpec` allows transforming docblock type trees.
* Passing an empty string to the parser will now throw a `MalformedTypeSpecificationException`.

## 0.1.0
* Initial release.
